package ru.inordic.javakid.ivan;

import java.util.Scanner;

public class HW3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        float[] numberCircle = new float[5];
        int i;
        int resultat;
        for (i = 0; i < numberCircle.length; i++) {
            System.out.println("===========================");
            System.out.println("Пошёл цикл № : " + (i + 1));
            System.out.println("===========================");
            System.out.println("Выберите режим работы: c целыми числами введите - 0, с дробными - 1, Завершение работы - 2");
            int vV = scanner.nextInt();
            if (vV == 2) {
                break;
            } else {

                if (vV == 0) {
                    System.out.println("===========================");
                    System.out.println("Выбран режим работы: c целыми числами");
                    System.out.println("===========================");
                    System.out.println("Введите первое число");
                    int x1 = scanner.nextInt();
                    System.out.println("Введите второе число");
                    int x2 = scanner.nextInt();
                    System.out.println("Введите код операции (0 - сумма, 1 - разность, 2 - произведение, 3 - деление, 4 - сравнение, 5 - сдвиг)");
                    int dev = scanner.nextInt();
                    boolean c;
                    switch (dev) {
                        case 0:
                            resultat = x1 + x2;
                            numberCircle[i] = resultat;
                            System.out.println("===========================");
                            System.out.println("Выбрано действие 'СЛОЖЕНИЕ', результат равен   " + resultat);

                            break;
                        case 1:
                            resultat = x1 - x2;
                            numberCircle[i] = resultat;
                            System.out.println("===========================");
                            System.out.println("Выбрано действие 'РАЗНОСТЬ', результат равен   " + resultat);

                            break;
                        case 2:
                            resultat = x1 * x2;
                            numberCircle[i] = resultat;
                            System.out.println("===========================");
                            System.out.println("Выбрано действие 'УМНОЖЕНИЕ', результат равен   " + resultat);

                            break;
                        case 3:
                            if (x2 == 0) {
                                System.out.println("===========================");
                                System.out.println("Выбрано действие 'ДЕЛЕНИЕ', но делить на 0 НЕЛЬЗЯ");
                                boolean isOK = false;
                                do {
                                    System.out.println("===========================");
                                    System.out.println("Введите другое число");
                                    int del = scanner.nextInt();
                                    if (del != 0) {
                                        isOK = true;
                                        resultat = x1 / del;
                                        numberCircle[i] = resultat;
                                        System.out.println("===========================");
                                        System.out.println("Теперь можно поделить, результат равен   " + resultat);
                                    } else {
                                        System.out.println("===========================");
                                        System.out.println("Снова число равно 0, попробуйте ещё");
                                    }
                                } while (!isOK);
                            } else {
                                resultat = x1 / x2;
                                numberCircle[i] = resultat;
                                System.out.println("===========================");
                                System.out.println("Выбрано действие 'ДЕЛЕНИЕ', результат равен   " + resultat);

                            }
                            break;
                        case 4:
                            System.out.println("===========================");
                            System.out.println("Выбрано действие 'СРАВНЕНИЕ', сравниваются два числа");
                            System.out.println("Первое число:  " + x1);
                            System.out.println("Второе число:  " + x2);
                            System.out.println("===========================");
                            if (c = x1 == x2) {
                                System.out.println(" Числа равны ");
                            } else {
                                if (c = x1 > x2) {
                                    System.out.println("Первое число больше второго  ");
                                    System.out.println(c);
                                    System.out.println(("Первое число:  " + x1) + (" больше второго:  " + x2));
                                } else {
                                    System.out.println("Первое число больше второго  ");
                                    System.out.println(c);
                                    System.out.println(("Второе число:  " + x2) + ("   больше первого:  " + x1));
                                }
                            }
                            break;
                        case 5:
                            System.out.println("===========================");
                            System.out.println("Выбран оператор 'СДВИГА' ");
                            System.out.println("===========================");
                            System.out.println("Выберите 'Варинат СДВИГА': 0-влево << , 1-вправо >> , 2-беззнакового сдвига вправо >>> ");
                            int varCdvig = scanner.nextInt();
                            if (varCdvig == 0) {
                                resultat = x1 << x2;
                                numberCircle[i] = resultat;
                                System.out.println("===========================");
                                System.out.println("Выбран оператор 'СДВИГА' влево << ");
                                System.out.println("===========================");
                                System.out.println(("Число: " + x1 + " ") + ("Сдвигается на " + x2));
                                System.out.println(("Значение числа : " + x1) + " со сдвигом влево на " + x2 + (" равно ") + resultat);

                            }
                            if (varCdvig == 1) {
                                resultat = x1 >> x2;
                                numberCircle[i] = resultat;
                                System.out.println("===========================");
                                System.out.println("Выбран оператор 'СДВИГА' вправо >> ");
                                System.out.println("===========================");
                                System.out.println(("Число: " + x1 + " ") + ("Сдвигается на " + x2));
                                System.out.println(("Значение числа : " + x1) + " со здвигом вправо на " + x2 + (" равно ") + resultat);

                            }
                            if (varCdvig == 2) {
                                resultat = x1 >>> x2;
                                numberCircle[i] = resultat;
                                System.out.println("===========================");
                                System.out.println("Выбран оператор 'БЕЗЗНАКОВОГО СДВИГА' вправо >>> ");
                                System.out.println("===========================");
                                System.out.println(("Число: " + x1 + " ") + ("Сдвигается на " + x2));
                                System.out.println(("Значение числа : " + x1) + " с беззнаковым сдвигом вправо на " + x2 + (" равно ") + resultat);

                            }
                            break;
                    }
                }

                if (vV == 1) {
                    System.out.println("===========================");
                    System.out.println("Выбран режим работы: c дробными числами");
                    System.out.println("===========================");
                    System.out.println("Введите первое число");
                    float x1 = scanner.nextFloat();
                    System.out.println("Введите второе число");
                    float x2 = scanner.nextFloat();
                    System.out.println("Введите код операции (0 - сумма, 1 - разность, 2 - произведение, 3 - деление)");
                    int dev = scanner.nextInt();
                    switch (dev) {
                        case 0:
                            numberCircle[i] = x1 + x2;

                            System.out.println("===========================");
                            System.out.println("Выбрано действие 'СЛОЖЕНИЕ', результат равен   " + numberCircle[i]);
                            break;
                        case 1:
                            numberCircle[i] = x1 - x2;
                            System.out.println("===========================");
                            System.out.println("Выбрано действие 'РАЗНОСТЬ', результат равен   " + numberCircle[i]);
                            break;
                        case 2:
                            numberCircle[i] = x1 * x2;
                            System.out.println("===========================");
                            System.out.println("Выбрано действие 'УМНОЖЕНИЕ', результат равен   " + numberCircle[i]);
                            break;
                        case 3:
                            if (x2 == 0) {
                                System.out.println("===========================");
                                System.out.println("Выбрано действие 'ДЕЛЕНИЕ', но делить на 0 НЕЛЬЗЯ");
                                boolean isOK = false;
                                do {
                                    System.out.println("===========================");
                                    System.out.println("Введите другое число");
                                    float del = scanner.nextFloat();
                                    if (del != 0) {
                                        isOK = true;
                                        System.out.println("===========================");
                                        System.out.println("Теперь можно поделить, результат равен   " + (x1 / del));
                                    } else {
                                        System.out.println("===========================");
                                        System.out.println("Снова число равно 0, попробуйте ещё");
                                    }
                                } while (!isOK);
                            } else {
                                numberCircle[i] = x1 / x2;
                                System.out.println("===========================");
                                System.out.println("Выбрано действие 'ДЕЛЕНИЕ', результат равен   " + numberCircle[i]);
                            }
                            break;
                    }
                }

            }

        }
        if (i != 0) {
            System.out.println("===========================");
            System.out.println("Напишите номер операции, результат которой хотите увидеть ");
            i = scanner.nextInt();
            System.out.println("===========================");
            System.out.println("Номер операции " + (i+1) + " результат " + numberCircle[i]);
            System.out.println("===========================");
            System.out.println("Завершение работы");
        } else {
            System.out.println("===========================");
            System.out.println("Завершение работы");
        }
    }
}

